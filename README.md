Vocabulary Condition
===

Provides a Taxonomy Vocabulary condition for block visibility.

This condition can be used when configuring visibility rules for blocks on the
Blocks Layout page, or when configuring conditions with the Context module.

The context service within this module loads the taxonomy term from the current
page's route. The condition then determines if the vocabulary of that taxonomy
term is one that was selected in the configuration. Note that this means that
they are essentially answering "Is this the page callback for a term of this
vocabulary?" and _not_ "Is the current page's node tagged to a term from this
vocabulary?" or anything like that.

The condition and context are based _heavily_ on the `NodeType` condition and
`NodeRouteContext` in core. There are multiple issues to get a similar condition
into core, and if/when that happens this module will be deprecated, but none of
those issues currently have a clear direction and recent activity.

They include:

* https://www.drupal.org/node/2281659
* https://www.drupal.org/node/2288861
* https://www.drupal.org/node/1932810

Related modules that have stemmed from those issues but do not currently work:

* https://www.drupal.org/project/entity_block_visibility
* https://github.com/md-systems/vocabulary_condition
